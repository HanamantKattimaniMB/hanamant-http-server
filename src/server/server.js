const http=require('http')
const fs=require('fs')
const uuid=require('uuid4')

const port = 3000
const PATH_INDEX_HTML = '../public/index.html'

function fileRead(filename){
    return new Promise((resolve,reject) => {
        fs.readFile(filename,(err,data) => {
            if(err){
                reject('Error : not able to read file')
            }else{
                resolve(data)
            }
        })
    })
}

function uuidGenerator(){
    let uid = uuid()
    const uidObj = {'uuid':uid}
    const uuidJson = JSON.stringify(uidObj)
    return uuidJson
}


function errorHandler(response,error){
    console.log(error)
    response.writeHead(200,{'Content-Type':'text/html'})
    response.write('<h1><center>Requested file is not present in server</center></h1>')
    response.end()
}

const server=http.createServer((request,response) => {
    let checkURL
    if(!request.url.search('/status')){
        checkURL = '/status'
    }else{
        if(!request.url.search('/delay')){
            checkURL = '/delay'
        }else{
            checkURL = request.url
        }
    }

    switch(checkURL){
        case '/html':{
            fileRead(PATH_INDEX_HTML)
            .then((homePageContent) => {
                response.writeHead(200,{'Content-Type':'text/html'})
                response.write(homePageContent)
                response.end()
            })
            .catch((error) => {
                errorHandler(response,error)
            })
            break
        }

        case '/json':{
            fileRead('../data/slideShow.json')
            .then((slideShowData) => {
                response.writeHead(200,{'Content-Type':'application/json'})
                response.write(slideShowData)
                response.end()
            })
            .catch((error) => {
                errorHandler(response,error)
            })
            break
        }

        case '/uuid':{
            const uuidOfJson = uuidGenerator()
            response.writeHead('200',{'Content-Type':'application/json'})
            response.write(uuidOfJson)
            response.end()
        break
        }

        case '/status':{
            let statusCodeWithURL = request.url
            let statusCode = statusCodeWithURL.slice(8)

            if((statusCode == 200) || (statusCode == 300) || (statusCode == 400) || (statusCode == 500)){
                response.writeHead(statusCode,{'Content-Type':'text/plain'})
                response.write('response on '+statusCode)
                response.end()
            }else{
                response.write('Wrong status code request')
                response.end()
            }
        break
        }

        case '/delay':{
            delayWithURL = request.url
            delay = delayWithURL.slice(7)
            
            setTimeout(() => {
                response.writeHead('200',{'Content-Type':'application/json'})
                if(!(delay == '')){
                response.write('Server gave response with '+delay+' seconds delay')
                }else{
                    response.write('You not mentioned delay')
                }
                response.end()
            },delay*1000)  
        break
        }

        default:{
            response.writeHead(404,{'Content-Type':'text/html'})
            response.write("<h1><center>Error: 404 not found</center></h1>")
            break
        }
    }
})

server.listen(port,(err) => {
    if(err){
        console.log("Error to listen on port number ",port)
    }else{
        console.log("Server is running on port :",port)
    }
})